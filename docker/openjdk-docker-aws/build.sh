#!/usr/bin/env bash

cd "$(dirname "${BASH_SOURCE[0]}")" || exit

java_versions=(15 16 17)
node_versions=(16)

aws ecr-public get-login-password --region us-east-1 | docker login --username AWS --password-stdin public.ecr.aws/j2k8f4r6
for version in "${java_versions[@]}"; do
  for node_version in "${node_versions[@]}"; do
    docker build -t public.ecr.aws/j2k8f4r6/openjdk-node-yarn:"$version-node_$node_version"-slim --build-arg JAVA_VERSION="$version" NODE_VERSION="$node_version" .
    docker push public.ecr.aws/j2k8f4r6/openjdk-node-yarn:"$version-node_$node_version"-slim
  done
done
