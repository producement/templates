#!/usr/bin/env bash

cd "$(dirname "${BASH_SOURCE[0]}")" || exit

java_versions=(17)
node_versions=(16)
gradle_versions=("7.3")

aws ecr-public get-login-password --region us-east-1 | docker login --username AWS --password-stdin public.ecr.aws/j2k8f4r6
for version in "${java_versions[@]}"; do
  for node_version in "${node_versions[@]}"; do
    for gradle_version in "${gradle_versions[@]}"; do
      docker build -t public.ecr.aws/j2k8f4r6/gradle-node-yarn:"$gradle_version-jdk$version-node$node_version"-slim --build-arg JAVA_VERSION="$version" --build-arg NODE_VERSION="$node_version" --build-arg GRADLE_VERSION="$gradle_version" .
      docker push public.ecr.aws/j2k8f4r6/gradle-node-yarn:"$gradle_version-jdk$version-node$node_version"-slim
    done
  done
done
